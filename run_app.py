import pandas as pd

from sites.isobench import Iso
from sites.coinmarketcap import CoinMC
from sites.cointelegraph import CoinTg
from sites.bravenewcoin import Brave
from sites.terrapin import Terrapin
from sites.inomics import Inomics
from sites.tentimes import TenTimes


def get_dfs():

    items = [Iso(), 
            CoinMC(), 
            CoinTg(), 
            Brave(), 
            Terrapin('finance'), 
            Terrapin('technology'),
            TenTimes()]

    dfs = []

    for item in items:
        try:
            dfs.append(item.run())
            print(' success --- %s ' % item.HOST)
            
        except:
            print(' error --- %s ' % item.HOST)
    
    return dfs


def concat_dfs():
    dfs = get_dfs()
    df = pd.concat(dfs)
    df.reset_index(inplace = True)
    del df['index']
    return df


def save_dfs():
    concat_dfs().to_excel('/Users/andrejalekseev/Desktop/events.xlsx')



if __name__ == "__main__":
    #print(TenTimes().run())
    save_dfs()

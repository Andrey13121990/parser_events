## Events parser 

---

**Implemented:**

- [Site](https://coinmarketcap.com/ru/events/) --- [file](./sites/coinmarketcap.py) --- coinmarketcap
- [Site](https://cointelegraph.com/events) --- [file](./sites/cointelegraph.py) --- cointelegraph
- [Site](http://bravenewcoin.com/events) --- [file](./sites/bravenewcoin.py) --- bravenewcoin
- [Site](https://www.terrapinn.com/events/all/finance) --- [file](./sites/terrapin.py) --- terrapin
- [Site](https://10times.com/top100/finance) --- [file](./sites/tentimes.py) --- 10times
- [Site](https://icobench.com/icos) --- [file](./sites/isobench.py) --- isobench

---

**Not implemented:**
- [Site](https://www.crunchbase.com/sitemap/events) --- crunchbase
- [Site](http://icoholder.com/events) --- icoholder
- [Site](https://www.eventbrite.co.uk/d/united-kingdom--london/finance-conference/)  --- eventbrite

---

**Not correspond:**
- [Site](https://inomics.com/top/finance/events) --- [file](./sites/inomics.py) --- inomics
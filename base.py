import pandas as pd 
import datetime as dt


class ParseBase:
    def __init__(self, industry):
        self.URL = None
        self.HOST = None


    def get_results(self):
        raise NotImplementedError()


    def run(self):
        results = self.get_results()
        df = pd.DataFrame.from_dict(results).T
        df['Start']= pd.to_datetime(df['Start'])
        df['End']= pd.to_datetime(df['End'])
        return df[df['End'] > dt.datetime.now()]
from time import sleep
from parser import get_soup
from base import ParseBase


class CoinMC (ParseBase):
    def __init__(self):
        self.URL = 'https://coinmarketcap.com/ru/events/'
        self.HOST = 'https://coinmarketcap.com'
        self.sleep_time = 0.1


    def get_source_link(self, URL):
        tags = get_soup(URL).find_all('div', class_= "cal-event-detail")
        link = tags[0].find_all('div', class_ = 'cmc-bottom-margin-1x')

        result = []

        for item in link:
            try:
                result.append(item.find('a').get('href'))
            except:
                pass
        return result[-1]


    def get_base_info(self, tag):

        data = tag.find('div', class_= "cal-event__name")

        event = data.text
        site_url = self.HOST + data.find('a').get('href')
        link = self.get_source_link(site_url)

        sleep(self.sleep_time)

        return event, link


    def get_other_info(self, tag):
        data = tag.find('div', class_= "cal-event__meta").text

        m = data.replace('-', '•').split(sep = '•')
        try:
            city, country = m[2].split(sep = ', ')
        except:
            try:
                city, country = m[2], None
            except:
                city, country = None, None

        start, end = m[0], m[1]

        return start, end, country, city 


    def get_results(self):
        tags = get_soup(self.URL).find_all('div', class_= "cal-event__right-col")
        results = dict()

        for i, item in enumerate(tags):
            
            data = dict()
            data['Source'] = self.HOST
            data['Event'], data['Link'] = self.get_base_info(item)
            data['Start'], data['End'], data['Country'], data['City'] = self.get_other_info(item)
            results[i] = data
        return results
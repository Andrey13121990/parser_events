from time import sleep
from parser import get_soup
from base import ParseBase


class Terrapin (ParseBase):
    def __init__(self, industry):
        self.industry = industry
        self.URL = self.urls_per_industry()
        self.HOST = 'https://www.terrapinn.com'


    def urls_per_industry(self):
        industries =  dict(
            finance = 'https://www.terrapinn.com/events/all/finance',
            technology = 'https://www.terrapinn.com/events/all/technology'
        )
        return industries[self.industry]


    def get_base_info(self, tag):

        urls = tag.find_all('a')
        event = urls[0].get('title').split(sep = '-')[0]
        link = urls[0].get('href').split(sep = '?')[0]
        return event, link


    def get_dates(self, tag):

        dates = tag.find_all('h6')[0].text.replace('th', '') 
        dates = dates.replace('-', ' ').split(' ')
            
        if len (dates) == 3:
            start = dates[0] + ' ' + dates[1] + ' ' + dates[2]
            end = start
        
        elif len (dates) == 4:
            start = dates[0] + ' ' + dates[2] + ' ' + dates[3]
            end = dates[1] + ' ' + dates[2] + ' ' + dates[3]
        
        else:
            start = None
            end = None

        return start, end


    def get_place(self, tag):

        place = tag.find_all('p')
        country= None
        city = place[1].text

        return country, city


    def get_results(self):

        tags = get_soup(self.URL).find_all('div', class_= "row event-info")

        results = dict()

        for i, item in enumerate(tags):
            data = dict()
            
            data['Source'] = self.HOST
            data['Event'], data['Link'] = self.get_base_info(item) 
            data['Start'], data['End'] = self.get_dates(item)
            data['Country'], data['City'] = self.get_place(item)
            
            results[i] = data

        return results



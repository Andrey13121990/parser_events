import pandas as pd
import json

from parser import get_request
from base import ParseBase



class CoinTg (ParseBase):
    def __init__(self):
        self.URL = 'https://cointelegraph.com/api/v1/events'
        self.HOST = 'https://cointelegraph.com'

        self.params = {
            'limit': 50
            }


    def get_results(self):

        r = get_request(self.URL, params = self.params)
        js = json.loads(r)

        results = dict()
        for i, item in enumerate(js['events']):
            data = dict()
            data['Source'] = self.HOST
            data['Event'] = item['title']
            data['Link'] = item['website'].split('?')[0]
            data['Start'] = item['start_date'][:10]
            data['End'] = item['end_date'][:10]
            try:
                data['Country'] = item['country']['title']
            except:
                data['Country'] = item['country']
            
            try:
                data['City'] = item['city']['title']
            except:
                data['City'] = item['city']
            
            results[i] = data

        return results
import json
import datetime as dt

from parser import get_request
from base import ParseBase


class Brave(ParseBase):
    def __init__(self):
        self.URL = 'https://bravenewcoin.com/system/api/content/event'
        self.HOST = 'https://bravenewcoin.com'

        self.params = {
            'order': '-fields.startDate',
            'limit': 50,
            }


    def clean_place(self, data):
        res = []
        for item in data:
            if item not in ['',' ', 'event', 'Event']:
                res.append(item)
        return res


    def concatinate(self, data):
        res = ''
        for items in data[:-1]:
            res = res + items + ' ' 
        return res


    def get_place(self, tag):
        place = self.clean_place(tag['locationName'].replace(' ', ',').split(sep = ','))
        
        if len(place) > 1:
            country = place[-1]
            city = self.concatinate(place)
        
        else:
            country = place[-1]
            city = None
    
        return country, city


    def get_results(self):

        r = get_request(self.URL, params = self.params)
        js = json.loads(r)

        results = dict()
        for i, fields in enumerate(js['items']):
            item = fields['fields']

            data = dict()
            data['Source'] = self.HOST
            data['Event'] = item['name']
            data['Link'] = item['website']
            data['Start'] = item['startDate'][:10]
            data['End'] = item['endDate'][:10]
            data['Country'], data['City'] = self.get_place(item)
        
            results[i] = data

        return results
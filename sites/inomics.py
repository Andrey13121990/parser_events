import pandas as pd
from parser import get_request, get_soup
import datetime as dt

class Inomics():
    def __init__(self):
        self.URL = 'https://inomics.com/top/finance/events'
        self.HOST = 'https://inomics.com/'
        self.num_of_pages = 3
        self.year = 2021


    
    def get_event_data(self, page):

        params = {
            'page': page
            }

        return get_soup(self.URL, params = params).find_all('ul', class_ = "posts")


    def get_page(self, page):
        results = dict()

        posts = self.get_event_data(page)[0].find_all('a')
        
        for i, item in enumerate(posts):

            try:
                item.find_all('span', class_ = 'informations')[0]
            
                data = dict()

                data['Source'] = self.HOST
                data['Event'] = item.find('h2').text.replace('\n', ':').split(sep = ':')
                data['Event'] = data['Event'][1]
                
                t = item.find_all('span', class_ = 'informations')[0].text.split()

                data['Link'] = self.HOST + item.get('href')

                try:    
                    data['Start'] = t[1] + ' ' + t[2] + ' ' + str(self.year)
                except:
                    pass

                try:
                    data['End'] = t[3] + ' ' + t[4] + ' ' + str(self.year)
                except:
                    pass
                
                try:
                    data['Country'] = " ".join(t[7:])
                except:
                    pass

                data['City'] = None

                results[i] = data
            
            except:
                pass
        
        return results


    def get_df(self, page):
        results = self.get_page(page)
        df = pd.DataFrame.from_dict(results).T
        df['Start']= pd.to_datetime(df['Start'], errors='coerce')
        df['End']= pd.to_datetime(df['End'], errors='coerce')
        return df

    
    def run(self):

        df = self.get_df(1)

        for item in range(1, self.num_of_pages):
            df = pd.concat([df, self.get_df(item)])

        return df.to_excel('/Users/andrejalekseev/Desktop/check_data.xlsx')
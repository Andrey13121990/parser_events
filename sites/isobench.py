import pandas as pd
import datetime as dt

from time import sleep

from parser import get_request, get_soup


class Iso:
    def __init__(self):
        self.URL = 'https://icobench.com/icos'
        self.HOST = 'https://icobench.com'
        self.num_of_pages = 3
        self.sleep_time = 0.1


    def get_source_link(self, URL):
        tags = get_soup(URL).find_all('div', class_= "financial_data")
        link = tags[0].find('a', class_ = 'button_big').get('href')
        return link.split(sep = '?')[0]


    def get_data(self, params):
        tags = get_soup(self.URL, params = params).find_all('div', class_= "content")
        events, links, hosts, countries, cities = list(), list(), list(), list(), list()

        for item in tags:

            site_url = self.HOST + item.find('a').get('href')
            source_url = self.get_source_link(site_url)
            links.append(source_url)
            sleep(self.sleep_time)

            events.append(item.find('a').text)

            hosts.append(self.HOST)
            try:
                c = item.find('p').text.split(sep = '|')[2]
                countries.append(c.split(sep = ':')[1])
            except:
                countries.append(None)

            cities.append(None)
        
        return events, links, hosts, countries, cities


    def get_page(self, page):

        params = {
            'page': page,
            'filterSort': 'ending-desc'
            }

        r = get_request(self.URL, params = params)

        df = pd.read_html(r)[0]
        df['Event'], df['Link'], df['Source'], df['Country'], df['City'] = self.get_data(params)

        df['Start'] = pd.to_datetime(df['Start'], errors='coerce')
        df['End'] = pd.to_datetime(df['End'], errors='coerce')

        return df[['Source', 'Event', 'Link', 'Start', 'End', 'Country', 'City']]


    def run(self):

        df = self.get_page(1)

        for item in range(1, self.num_of_pages):
            df = pd.concat([df, self.get_page(item)])

        return df[df['End'] > dt.datetime.now()]
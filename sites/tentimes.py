import pandas as pd
import datetime as dt
import numpy as np

from parser import get_request, get_soup


class TenTimes:
    def __init__(self):
        self.URL = 'https://10times.com/top100/finance'
        self.HOST = 'https://10times.com/top100/finance'


    def get_refs(self):

        tags = get_soup(self.URL).find_all('table', class_= "table table-responsive table-hover")

        a = tags[0].find_all('td', class_ = 'box-link')

        links = []
        source = []
        for item in a:
            try:
                links.append(item.find('a').get('href'))
                source.append(self.HOST)
            except:
                pass

        return links, source


    def get_city_and_country(self, item):
        try:
            data = item.split(sep = " ")
        except:
            data = [None, None]
        
        if len(data) == 1:
            return [None, data[0]]
        
        else:
            return [data[0], data[-1]]

        
    def city(self, item):
        return self.get_city_and_country(item)[0]


    def country(self, item):
        return self.get_city_and_country(item)[1]


    def get_start_end_dates(self, item):
        try:
            a = item.split(sep = ' ')

            if len(a) == 3:
                return [" ".join(a), " ".join(a)]

            elif a[-1] == 'cancelled':
                return [np.NaN, np.NaN]

            elif len(a) == 5:
                return [a[0] + ' ' + a[3] + ' ' + a[4],
                        a[2] + ' ' + a[3] + ' ' + a[4]]

            elif a[-1] == 'postponed':
                if len(a) == 4:
                    return [" ".join(a), " ".join(a)]

                if len(a) == 6:
                    return [a[0] + ' ' + a[3] + ' ' + a[4],
                            a[2] + ' ' + a[3] + ' ' + a[4]]
                else:
                    return [None, None]

            else:
                return [None, None]

        except:
            return [None, None]


    def start(self, item):
        return self.get_start_end_dates(item)[0]


    def end(self, item):
        return self.get_start_end_dates(item)[1]


    def clean_rank(self, item):
        try: 
            a = int(item)   
        except:
            a = None  
        return a
        

    def run(self):
        df = pd.read_html(get_request(self.URL))[0].dropna(subset = ['Event'])
        df['tmp'] = df['Rank'].apply(self.clean_rank)
        df = df.dropna(subset = ['tmp'])
        del df['tmp']

        df['City'] = df['Where'].apply(self.city)
        df['Country'] = df['Where'].apply(self.country)

        df['Link'], df['Source'] = self.get_refs()

        df['Start'] = df['When'].apply(self.start)
        df['End'] = df['When'].apply(self.end)

        df['Start']= pd.to_datetime(df['Start'], errors='coerce')
        df['End']= pd.to_datetime(df['End'], errors='coerce')
        
        df = df[['Source', 'Event' ,'Link' ,'Start' ,'End' ,'Country' ,'City']].dropna(subset=['Start'])

        return df[df['End'] > dt.datetime.now()]

import requests
from bs4 import BeautifulSoup
import urllib.request


def get_headers_with_cookies(URL):

    r = urllib.request.urlopen(URL)
    ck = r.getheader('Set-Cookie')

    headers = {                                                                                                                                                         
        "Accept": "text/html,application/xhtml+xml,application/xml,application/json,text/plain;q=0.9,*/*;q=0.8;",
        "Cookie": ck,
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.2 Safari/605.1.15"
        }
    return headers


def get_headers():
    
    headers = {                                                                                                                                                         
        "Accept": "text/html,application/xhtml+xml,application/xml,application/json,text/plain;q=0.9,*/*;q=0.8;",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.2 Safari/605.1.15"
        }
    return headers


def get_request(URL, is_cookies = False, params = ''):
    
    if is_cookies:
        headers = get_headers_with_cookies(URL)
      
    else:
        headers = get_headers()
    
    return requests.get(URL, headers = headers, params = params).text


def get_soup(URL, is_cookies = False, params = '', parser = 'lxml'):
    
    r = get_request(URL, is_cookies, params = params)
    return BeautifulSoup(r, parser)